<?php

/**
 * @file
 * A class used for entity_rate.
 */

/**
 * The class used for Entity Rate entities.
 */
class EntityRate extends Entity {

  /**
   * The unique ID of this entity.
   *
   * @var string
   */
  public $rid;

  /**
   * The machine-name of this entity.
   *
   * @var string
   */
  public $name;

  /**
   * The human-readable label for this entity.
   *
   * @var string
   */
  public $label;

  /**
   * The basic entity type.
   *
   * @var string
   */
  public $entity_type;

  /**
   * The basic entity bundle.
   *
   * @var string
   */
  public $entity_bundle;

  /**
   * The options and configuration of this entity.
   *
   * @var string
   */
  public $options;

  public function __construct($values = array()) {
    parent::__construct($values, 'entity_rate');
  }

  /**
   * Override this in order to implement a custom default URI and specify
   * 'entity_class_uri' as 'uri callback' hook_entity_info().
   */
  protected function defaultUri() {
    return array('path' => 'entity_rate/' . $this->identifier());
  }

}

/**
 * The Controller for the Basic Entity (CRUD).
 */
class EntityRateAPIControllerExportable extends EntityAPIControllerExportable {

  /**
   * Implements EntityAPIControllerInterface.
   */
  public function create(array $values = array()) {
    return parent::create($values);
  }

  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $content
   *   Optionally. Allows pre-populating the built content to ease overridding
   *   this method.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity, $view_mode, $langcode, $content);
    dpm('EntityRateAPIControllerExportable buildContent');
    dpm($entity);
    $build['view_mode'] = array(
        '#type' => 'markup',
        '#markup' => $view_mode,
    );
    return $build;
  }
}

/**
 * UI controller
 */
class EntityRateUIController extends EntityDefaultUIController {

  /**
   * Overridden to customize the field location.
   */
  public function entityFormSubmitBuildEntity($form, &$form_state) {

    // We cannot use entity_form_submit_build_entity() any more.
    // We have extra fields which needs to be saved.
    $entity = $form_state['bmj_identity'];

    // Extract form values.
    form_state_values_clean($form_state);

    foreach ($form_state['values'] as $key => $value) {
      if ($key != 'extra') {
        $entity->$key = $value;
      }
    }

    // Invoke all specified builders for copying form values to entity
    // properties.
    // @see entity_form_submit_build_entity()
    if (isset($form['#entity_builders'])) {
      foreach ($form['#entity_builders'] as $function) {
        $function('bmj_identity', $entity, $form, $form_state);
      }
    }

    field_attach_submit('bmj_identity', $entity, $form['extra'], $form_state);

    return $entity;
  }

}

<?php

/**
 * @file
 * Provides support for the Views module.
 */

/**
 * "EntityRating" entity Views definition.
 */
class EntityRatingViewsController extends EntityDefaultViewsController {

  /**
   * Override views_data().
   */
  public function views_data() {
    $data = parent::views_data();
    return $data;
  }
}

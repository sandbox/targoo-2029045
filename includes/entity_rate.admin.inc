<?php

/**
 * @file
 * Contains pages for creating, editing, and deleting Entity Mail/Entity Mail Type.
 */

/**
 * Generate the Entity Basic Type editing form.
 */
function entity_rate_form($form, &$form_state, $entity_rate, $op = 'edit') {
  $field_language = NULL;

  if ($op == 'clone') {
    $entity_rate->type  = '';
    $entity_rate->name .= ' (cloned)';
  }

  // We will have many fields with the same name, so we need to be able to
  // access the form hierarchically.
  $form['#tree'] = TRUE;

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => $entity_rate->label,
    '#description' => t('The human-readable name of this basic entity type.'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Name'),
    '#default_value' => $entity_rate->name,
    '#description' => t('A unique machine-readable name for this basic entity type. It must only contain lowercase letters, numbers, and underscores.'),
    '#maxlength' => 64,
    '#machine_name' => array(
      'exists' => 'entity_rate_load',
      'source' => array('label'),
    ),
    '#required' => TRUE,
    '#disabled' => FALSE,
  );

  $types = array();
  foreach (entity_rate_get_entity_info() as $type => $info) {
    $types[$type] = $info['label'] . ' - ' . $info['description'];
  }
  $form['entity'] = array(
    '#type' => 'fieldset',
    '#title' => t('Entity'),
    '#description' => t('Define the available voting buttons.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['entity']['entity_type'] = array(
    '#type' => 'select',
    '#title' => t('Entity'),
    '#default_value' => 'node',
    '#description' => t('The type of entity this rate will affect. This cannot be changed once the rate is created.'),
    '#required' => TRUE,
    '#options' => $types,
  );
  foreach (entity_rate_get_entity_info() as $type => $info) {
    if (isset($info['bundles'])) {
      $form['entity'][$type]['bundle'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Bundles'),
        '#description' => t('The type of bundle this rate will affect. This cannot be changed once the rate is created.'),
        '#required' => FALSE,
        '#options' => $info['bundles'],
        '#states' => array(
          'visible' => array(
            ':input[name="entity[entity_type]"]' => array('value' => $type),
          ),
        ),
      );
    }
  }

  // Options.
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Options'),
    '#description' => t('Define the available rating values.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Container for just the rate choices.
  $form['options']['options'] = array(
    '#theme' => 'entity_rate_admin_options',
    '#prefix' => "<div id=\"entity-rate-options\">",
    '#suffix' => '</div>',
  );
  if ($form_state['submitted']) {
    $option_count = (int) $form_state['values']['options']['option_count'];
    if ($form_state['triggering_element']['#value'] == t('Add another option')) {
      ++$option_count;
    }
  }
  else {
    $option_count = 2;
  }
  for ($delta = 0; $delta < $option_count; $delta++) {
    $form['options']['options']['option' . $delta] = array();
    if (isset($form_state['values']['value' . $delta])) {
      $default_value = $form_state['values']['value' . $delta];
      $default_label = $form_state['values']['label' . $delta];
      $default_label = $form_state['values']['description' . $delta];
    }
    elseif (isset($widget->options[$delta]) && !$form_state['submitted']) {
      $default_value = $widget->options[$delta][0];
      $default_label = $widget->options[$delta][1];
      $default_description = $widget->options[$delta][2];
    }
    else {
      $default_value = '';
      $default_label = '';
      $default_description = '';
    }
    $form['options']['options']['option' . $delta]['value' . $delta] = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#default_value' => $default_value,
      '#size' => 16,
    );
    $form['options']['options']['option' . $delta]['label' . $delta] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => $default_label,
      '#size' => 32,
    );
    $form['options']['options']['option' . $delta]['description' . $delta] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => $default_description,
      '#size' => 64,
    );
    $form['options']['options']['option' . $delta]['delete' . $delta] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete'),
      '#default_value' => FALSE,
    );
  }
  $form['options']['option_count'] = array(
    '#type' => 'hidden',
    '#value' => $option_count,
  );
  $form['options']['add_another'] = array(
    '#type' => 'submit',
    '#value' => t('Add another option'),
    '#ajax' => array(
      'callback' => 'entity_rate_form_ajax',
      'wrapper' => 'entity-rate-options',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  // Actions.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => ENTITY_RATE_ADMIN_PATH,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function entity_rate_form_validate($form, &$form_state) {
  // Validate machine readable name.
  if (!preg_match('/^[a-z\\_0-9]+$/i', $form_state['values']['name'])) {
    form_set_error('name', t('The machine readable name may only contain alphanumeric characters and underscores.'));
  }
}

/**
 * Form API submit callback for the type form.
 */
function entity_rate_form_submit(&$form, &$form_state) {
  if ($form_state['triggering_element']['#value'] == t('Add another option')) {
    $form_state['rebuild'] = TRUE;
    return $form_state['values'];
  }
  // Get entity type.
  $form_state['values']['entity_type'] = $form_state['values']['entity']['entity_type'];
  // Get entity bundle.
  $form_state['values']['entity_bundle'] = serialize($form_state['values']['entity'][$form_state['values']['entity']['entity_type']]['bundle']);
  // Get options.
  $form_state['values']['options'] = serialize($form_state['values']['options']['options']);
  $entity_rate = entity_ui_form_submit_build_entity($form, $form_state);
  unset($entity_rate->entity);
  $entity_rate->save();
  $form_state['redirect'] = ENTITY_RATE_ADMIN_PATH;
}

/**
 * AHAH callback for widget form.
 */
function entity_rate_form_ajax($form, $form_state) {
  return $form['options']['options'];
}

/**
 * Theme the options list in the widget form.
 *
 * @ingroup themeable
 */
function theme_entity_rate_admin_options($variables) {
  $element = $variables['element'];

  $header = array(
      t('Value'),
      t('Label'),
      t('Description'),
      t('Delete'),
  );
  $rows = array();
  foreach ($element as $name => $subelement) {
    if (preg_match('/^option([0-9]+)$/', $name, $match)) {
      $id = $match[1];
      unset($subelement['value' . $id]['#title']);
      unset($subelement['label' . $id]['#title']);
      unset($subelement['description' . $id]['#title']);
      unset($subelement['delete' . $id]['#title']);
      $rows[] = array(
        drupal_render($subelement['value' . $id]),
        drupal_render($subelement['label' . $id]),
        drupal_render($subelement['description' . $id]),
        drupal_render($subelement['delete' . $id]),
      );
    }
  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}

<?php

/**
 * @file
 * A class used for entity_rate.
 */

/**
 * The class used for Entity Rate entities.
 */
class EntityRating extends Entity {

  /**
   * The unique ID of this entity.
   *
   * @var string
   */
  public $rid;

  /**
   * The machine-name of this entity.
   *
   * @var string
   */
  public $name;

  /**
   * The human-readable label for this entity.
   *
   * @var string
   */
  public $label;

  /**
   * The basic entity type.
   *
   * @var string
   */
  public $entity_type;

  /**
   * The options and configuration of this entity.
   *
   * @var string
   */
  public $options;

  public function __construct($values = array()) {
    parent::__construct($values, 'entity_basic');
    if (!isset($this->uid)) {
      $this->uid = $GLOBALS['user']->uid;
    }
    if (!isset($this->timestamp)) {
      $this->timestamp = time();
    }
  }

  /**
   * Override this in order to implement a custom default URI and specify
   * 'entity_class_uri' as 'uri callback' hook_entity_info().
   */
  protected function defaultUri() {
    return array('path' => 'entity_rate/' . $this->identifier());
  }

}

/**
 * The Controller for the Basic Entity (CRUD).
 */
class EntityRatingAPIController extends EntityAPIController {

  /**
   * Implements EntityAPIControllerInterface.
   */
  public function create(array $values = array()) {
    return parent::create($values);
  }

  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $content
   *   Optionally. Allows pre-populating the built content to ease overridding
   *   this method.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    $build['view_mode'] = array(
        '#type' => 'markup',
        '#markup' => $view_mode,
    );

    return $build;
  }

}
